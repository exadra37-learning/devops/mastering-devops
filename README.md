# MASTERING DEVOPS

Following along [this video course](https://www.packtpub.com/virtualization-and-cloud/mastering-devops-video) from [Dave Mangot](https://twitter.com/davemangot?lang=en).

The birth of the DevOps movment may have started on the O'Reilly 2009 conference
with this talk [10+ Deploys Per Day: Dev and Ops Cooperation at Flickr](https://www.youtube.com/watch?v=LdOe18KhtT4).


Video Course Sections:

1. What is DevOps
2. The CAMS Model
3. Establishing a DevOps Culture
4. Configuration Management
5. Automation
6. Measurement
7. Sharing
8. Next Steps
